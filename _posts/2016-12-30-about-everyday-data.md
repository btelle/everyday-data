---
layout: post
title: About Everyday Data
---

I'm [Brandon](http://www.btelle.me), a Data Engineer at 
[Loot Crate](https://lootcrate.com). This blog is my attempt at examining the data we 
generate (and largely ignore) in our daily lives. I plan to use the projects posted here 
as a chance to learn new things about data extraction, cleansing, visualizations, and 
statistics. 

Posts on Everyday Data will all follow the same basic format: 

## Description

What is the data source being examined? How common is it? Does it apply to anyone but me? 
(Hopefully, yes!) Explain the question being explored and any initial hypotheses.

## Extraction

How is the data obtained? This section will typically include code snippets and sample 
data. 

## Analysis

Statistics and visualizations go here. Do the visualizations reveal anything unexpected? 
Explain the methodology behind the visualizations used. 

## Wrap up

Examine the original question and hypothesis. Was the hypothesis accurate? If not, why 
not? 

## Further reading

Links to source material, project code, visualization documentation, and statistics wiki 
pages. 

Interested? Subscribe to the [RSS Feed](/feed.xml) or 
[Twitter](https://twitter.com/nigerianbacon) for updates. 