---
layout: page
title: About
permalink: /about/
feature-img: "img/sample_feature_img_3.png"
---

Everyday Data is a blog about taking a closer look at everyday data. Each post 
focuses on some aspect of ordinary life that can be quantified and analyzed. 
Posts discuss how data is collected, organized, analyzed and displayed.

## About Brandon Telle

<div style="text-align: center;">
<img src="/img/brandon.jpg" alt="Brandon" />
</div>

[Brandon Telle](http://www.btelle.me) is a data engineer, web developer, 
python enthusiast. His endless curiosity with regards to data and daily 
routines lead him to start Everyday Data in 2017.
