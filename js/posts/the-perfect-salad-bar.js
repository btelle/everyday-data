d3.csv('/data/the-perfect-salad-bar/cities_by_ranking.csv', function(data) {
  nv.addGraph(function() {
    var chart = nv.models.discreteBarChart()
	  .x(function(d) { return d['city'] })    //Specify the data accessors.
	  .y(function(d) { return d['rating'] })
	  ;
	
	chart.xAxis
	    .axisLabel('City')
	    .rotateLabels(-25);
	
	chart.yAxis.axisLabel('Rating (★)');
	chart.margin().bottom = 75;
	
	chart.tooltip.contentGenerator(function (data) {
      var value = Math.round(data.data['rating'] * 100) / 100;
      return  "<p><strong>"  + value + "</strong></p>";
    });
    
    d3.select('#cities_by_ranking svg')
	  .datum([{values: data}])
      .transition().duration(500)
	  .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
  });
});

d3.csv('/data/the-perfect-salad-bar/salad_reviews.csv', function(data) {
  nv.addGraph(function() {
    var chart = nv.models.scatterChart()
		.showDistX(true)    //showDist, when true, will display those little distribution lines on the axis.
		.showDistY(true);
	
	chart.yAxis.axisLabel('Rating (★)');
	chart.xAxis.axisLabel('Distance from 91307 (m)');
	
	chart.tooltip.contentGenerator(function (data) {
      return  "<p><strong>"  + data.point['name'] + "</strong> in " + data.point['city'] + "</p>";
    });
    
    d3.select('#salad_reviews svg')
	  .datum(formatScatterData(data))
	  .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
  });
});

d3.text("/data/the-perfect-salad-bar/salad_sorted_restaurants.csv", function(data) {
	var parsedCSV = d3.csv.parseRows(data);

	var container = d3.select("#sorted_reviews")

		.selectAll("tr")
			.data(parsedCSV).enter()
			.append("tr")

		.selectAll("td")
			.data(function(d) { return d; }).enter()
			.append("td")
			.text(function(d) { return d; });
});

var formatScatterData = function(data) {
  ret = [{values:[]}]
  for(i=0; i<data.length; i++) {
	ret[0].values.push({
	  x: parseInt(data[i]['distance']),
	  y: parseFloat(data[i]['rating']),
      name: data[i]['name'],
      city: data[i]['city'],
      size: data[i]['price'].length * 1.5
    });
    console.log(ret[0].values[ret[0].values.length-1])
  }
  return ret;
}