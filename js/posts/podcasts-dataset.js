d3.json('/data/podcasts-dataset/audio-mime-types.json', function(data) {
	nv.addGraph(function() {
	  var chart = nv.models.pieChart()
		  .x(function(d) { return d.label })
		  .y(function(d) { return d.value/1248823 })
		  .showLabels(false)     //Display pie labels
		  .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
		  .donutRatio(0.25)     //Configure how big you want the donut hole size to be.
		  ;
		
		chart.tooltip.contentGenerator(function (data) {
		  return  "<p><strong>"  + data.data['label'] + "</strong>: "+data.data['value']+"</p>";
		});
		
		d3.select("#audio-mime svg")
			.datum(data)
			.transition().duration(350)
			.call(chart);

	  return chart;
	})
});

d3.csv('/data/podcasts-dataset/output_categories-over-time.csv', function(data) {
  nv.addGraph(function() {
	var chart = nv.models.multiBarChart()
				  .x(function(d) { return d[0] })
				  .y(function(d) { return d[1] })
				  .color(d3.scale.category10().range())
				  .stacked(true)
				  .showControls(false);
				  ;
	 chart.yAxis
	 	.tickFormat(function(d) {return d});
	 	
	 chart.xAxis
		.tickFormat(function(d) {
			return d3.time.format('%x')(new Date(d))
		  });
	
	chart.tooltip.contentGenerator(function (data) {
	  return  "<p><strong>" + data.data['key'] + " " + d3.time.format('%x')(new Date(data.data[0])) +  "</strong>: "+data.data[1]+"</p>";
	});

	d3.select('#categories-histogram svg')
		.datum(csv_to_histogram(data))
		.call(chart);

	return chart;
  });
});

var csv_to_histogram = function(data) {
	var categories = {};
	
	for(i in data) {
		row = data[i];
		
		if(!categories[row['category']]) {
			categories[row['category']] = [];
		}
		
		categories[row['category']].push([(new Date(row['date'])).getTime(), parseInt(row['count'])]);
	}
	
	var ret = [];
	for(key in categories) {
		ret.push({"key": key, "values": categories[key]});
	}
	
	return ret;
}