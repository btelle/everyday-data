d3.csv('/data/hacker-news-part-one/02_pay_by_experience.csv', function(data) {
  nv.addGraph(function() {
    var chart = nv.models.multiBarChart()
      //.transition().duration(350)
      .reduceXTicks(false)   //If 'false', every single x-axis tick label will be rendered
      .showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      .groupSpacing(0.1)    //Distance between each group of bars.
    ;

    chart.yAxis
        .tickFormat(d3.format(',.1f'))
        .axisLabel('Annual Pay ($)')
        .rotateLabels(-45);
    
    chart.xAxis
	    .axisLabel('Job Category')
	    .rotateLabels(-45);
	
	chart.margin().bottom = 95;
	
	chart.tooltip.contentGenerator(function (data) {
	  var value = Math.round(data.data.y * 100) / 100
      return "<p>"+data.data.x+", "+data.series[0].key+": $"+value+"</p>"
    });
	
    d3.select('#salary-job-title-rank svg')
        .datum(format_stacked_bar_data(data))
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
  });
});

d3.csv('/data/hacker-news-part-one/03_salary_by_experience_partition.csv', function(data) {
	/*These lines are all chart setup.  Pick and choose which chart features you want to utilize. */
	nv.addGraph(function() {
	  var chart = nv.models.lineChart()
					.margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
					.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
					//.transitionDuration(350)  //how fast do you want the lines to transition?
					.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
					.showYAxis(true)        //Show the y-axis
					.showXAxis(true)        //Show the x-axis
	  ;
	  var partitions = ['0-2', '2-4', '4-6', '6-8', '8-10+'];
	  
	  chart.xAxis     //Chart x-axis settings
		  .axisLabel('Experience (years)')
		  .tickValues([0, 1, 2, 3, 4, 5, 6])
		  .tickFormat(function(d){
			return partitions[d]
		  });

	  chart.yAxis     //Chart y-axis settings
		  .axisLabel('Annual Pay ($)')
		  .tickFormat(d3.format('.02f'));

	  d3.select('#experience-partition svg')    //Select the <svg> element you want to render the chart in.   
		  .datum(format_line_chart_data(data))         //Populate the <svg> element with chart data...
		  .call(chart);          //Finally, render the chart!

	  //Update the chart when window resizes.
	  nv.utils.windowResize(function() { chart.update() });
	  return chart;
	});
});

d3.csv('/data/hacker-news-part-one/04_popular_cities.csv', function(data) {
	var color = d3.scale.ordinal()
		.domain(["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"])
		.range(["#1ca502", "#7610fc", "#fe4d52", "#1698ce", "#6d5d2c", "#993490", "#19a17b", "#fc13e8", "#1053d9", "#cf790e", "#ac829a", "#ff3e97", "#89960e", "#9b423a", "#8c84da", "#406563", "#bd65f9", "#226d13", "#fa560d", "#4e5d8a", "#a92e60", "#ab3506", "#7b975e", "#c07f4e", "#ed4bc4", "#189da4", "#2445f6", "#958d7d", "#ca70ac", "#9115d4", "#18a350", "#e36673", "#7048b1", "#1c8ffe", "#7d544f", "#2c6a42", "#ac8a2a", "#7a91a5", "#7c4d85", "#a410ac", "#bb0f3c", "#0f5eb0", "#bc70d5", "#875207", "#e16b4a", "#897dfe", "#57640c", "#5e9e2d", "#c17c78", "#639982", "#8e4865"]);
		
	nv.addGraph(function() {
	  var chart = nv.models.discreteBarChart()
		  .x(function(d) { return d.label })    //Specify the data accessors.
		  .y(function(d) { return d.value })
		  .forceY([0, 66]);
	  
	  chart.yAxis     //Chart y-axis settings
		  .axisLabel('Jobs')
	  
	  chart.xAxis     //Chart y-axis settings
		  .axisLabel('City')
		  .tickValues(0);
	  
	  chart.tooltip.contentGenerator(function (data) {
		  return "<p>"+data.data.label+": "+data.data.value+"</p>"
	  });
	  
	  d3.select('#popular-cities svg')
		  .datum(format_bar_chart_data(data))
		  .call(chart);

	  nv.utils.windowResize(chart.update);

	  return chart;
	});
});

var format_stacked_bar_data = function(data) {
	ret = [{key: 'Pay Per Year Worked', values: []}, {key: 'Pay Per Year At Employer', values: []}];
	
	for (i=0; i<data.length; i++) {
		ret[0]['values'].push({
			x: data[i]['job_title_category'],
			y: data[i]['base_pay_dollars_per_year_worked']
		});
		
		ret[1]['values'].push({
			x: data[i]['job_title_category'],
			y: data[i]['base_pay_dollars_per_year_at_current_job']
		});
	}
	
	return ret;
};

var format_line_chart_data = function(data) {
	var ret = [];
	var series = {};
	
	var partitions = {'0-2': 0, '2-4': 1, '4-6': 2, '6-8': 3, '8-10+': 4};
	
	for(i=0; i< data.length; i++) {
		console.log(data[i]);
		if(!series[data[i]['job_title_category']]) {
			series[data[i]['job_title_category']] = [];
		}
		
		series[data[i]['job_title_category']].push({
			x: partitions[data[i]['experience_partition']],
			y: parseInt(data[i]['annual_base_pay'])
		});
	}
	
	console.log(series);
	
	Object.keys(series).map(function(key, index) {
		ret.push({
			key: key,
			values: series[key]
		});
	});
	
	return ret;
};

var format_bar_chart_data = function(data) {
	ret = [{key: 'Popular cities', values: []}];
	nodes = Object.keys(data).map(function(key, index) {
		ret[0]['values'].push({
			label: data[index]['location_name'],
			value: data[index]['salary_id'],
			state: data[index]['location_state'],
			color: color_map(data[index]['location_state'])
		});
	});
	console.log(ret);
	return ret;
};

var color_map = function(state) {
	var states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"];
	var colors = ["#1ca502", "#7610fc", "#fe4d52", "#1698ce", "#6d5d2c", "#993490", "#19a17b", "#fc13e8", "#1053d9", "#cf790e", "#ac829a", "#ff3e97", "#89960e", "#9b423a", "#8c84da", "#406563", "#bd65f9", "#226d13", "#fa560d", "#4e5d8a", "#a92e60", "#ab3506", "#7b975e", "#c07f4e", "#ed4bc4", "#189da4", "#2445f6", "#958d7d", "#ca70ac", "#9115d4", "#18a350", "#e36673", "#7048b1", "#1c8ffe", "#7d544f", "#2c6a42", "#ac8a2a", "#7a91a5", "#7c4d85", "#a410ac", "#bb0f3c", "#0f5eb0", "#bc70d5", "#875207", "#e16b4a", "#897dfe", "#57640c", "#5e9e2d", "#c17c78", "#639982", "#8e4865"];
	
	return colors[states.indexOf(state)]
}
