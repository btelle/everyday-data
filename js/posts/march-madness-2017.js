d3.csv('/data/march-madness-2017/accuracy.csv', function(data) {
	nv.addGraph(function() {
	  var chart = nv.models.pieChart()
		  .x(function(d) { return d.label })
		  .y(function(d) { return d.value })
		  .showLabels(true)     //Display pie labels
		  .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
		  .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
		  .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
		  .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
		  ;
		chart.tooltip.contentGenerator(function (data) {
		  return "<p>"+data.data.label+": "+data.data.value+"</p>"
		});
		d3.select("#accuracy svg")
			.datum(data)
			.transition().duration(350)
			.call(chart);

	  return chart;
	});
});

d3.csv('/data/march-madness-2017/rounds.csv', function(data) {
	nv.addGraph(function() {
	  var colors = d3.scale.ordinal()
	  	.range(['#87B8DB', '#65A4D1', '#3F8ABF', '#1F76B4', '#085E9A', '#064877']);
	  	
	  var chart = nv.models.discreteBarChart()
		  .x(function(d) { return d.label })    //Specify the data accessors.
		  .y(function(d) { return d.value })
		  .color(colors.range())
	  
	  chart.yAxis     //Chart y-axis settings
		  .axisLabel('Games Predicted Correctly (%)');
	  
	  chart.xAxis     //Chart x-axis settings
		  .axisLabel('Round');
	  
	  chart.tooltip.contentGenerator(function (data) {
		  var round = "Round of "+data.data.label;
		  if (data.data.label == 16) {
		  	round = 'Sweet Sixteen';
		  } else if (data.data.label == 8) {
		  	round = "Elite Eight";
		  } else if (data.data.label == 4) {
		  	round = "Final Four";
		  } else if (data.data.label == 2) {
		  	round = "Championship";
		  }
		  
		  return "<p>"+round+": "+data.data.raw+" ("+data.data.value+"%)</p>"
	  });
	  
	  d3.select('#rounds svg')
		  .datum(format_bar_chart_data(data))
		  .call(chart);

	  nv.utils.windowResize(chart.update);

	  return chart;
	});
});

d3.csv('/data/march-madness-2017/upsets.csv', function(data) {
	nv.addGraph(function() {
	  var chart = nv.models.pieChart()
		  .x(function(d) { return d.label })
		  .y(function(d) { return d.value })
		  .showLabels(true)     //Display pie labels
		  .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
		  .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
		  .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
		  .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
		  ;
		chart.tooltip.contentGenerator(function (data) {
		  return "<p>"+data.data.label+": "+data.data.value+"</p>"
		});
		d3.select("#upsets svg")
			.datum(data)
			.transition().duration(350)
			.call(chart);

	  return chart;
	});
});

var format_bar_chart_data = function(data) {
	var ret_arr = [ 
		{
		  key: "Predicted games by round",
		  values: []
		}
    ]
    
    for (i in data) {
    	ret_arr[0].values.push({'label': data[i]['round'], 'value': data[i]['accuracy_pct']*100, 'raw': data[i]['correct_games']});
    }
    
    return ret_arr;
};